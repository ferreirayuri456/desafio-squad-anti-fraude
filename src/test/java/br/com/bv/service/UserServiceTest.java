package br.com.bv.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

import br.com.bv.domain.User;
import br.com.bv.domain.UserDTO;
import br.com.bv.exception.BusinessException;
import br.com.bv.exception.DataNotFoundException;
import br.com.bv.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@DisplayName("Service - User")
public class UserServiceTest {

	@InjectMocks
	private UserService service;

	@Mock
	private UserRepository repository;

	@Nested
	@DisplayName("FIND BY ID")
	class whenGetById {

		@Test
		@DisplayName("FIND BY ID - When valid id - Expected sucess")
		public void getById_whenValid_expectedSucess() {
			when(repository.findById(1234L)).thenReturn(createOpcionalUser());
			var result = service.listClientByCPF(1234L);
			assertEquals(UserDTO.dtoToEntity(createUserDTO()), result);
		}

		@Test
		@DisplayName("FIND BY ID - When invalid id - Expected fail")
		public void getById_whenInvalid_expectedFail() {
			Assertions.assertThrows(DataNotFoundException.class, () -> {
				when(repository.findById(1234L)).thenReturn(Optional.empty());
				service.listClientByCPF(1234L);
			});
		}

	}

	@Nested
	@DisplayName("DELETE")
	class whenDelete {

		@Test
		@DisplayName("DELETE - When valid id - Expected sucess")
		public void delete_whenValidId_expectedSucess() {
			when(repository.findById(1234L)).thenReturn(Optional.of(createUser()));
			doNothing().when(repository).deleteById(1234L);
			service.removeClient(1234L);
			verify(repository).deleteById(1234L);
		}

		@Test
		@DisplayName("DELETE - When invalid id - Expected fail")
		public void delete_whenInvalidId_expectedBadRequest() {
			doThrow(new EmptyResultDataAccessException(1)).when(repository).findById(1234L);
			Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
				service.removeClient(1234L);
			});
		}

	}

	@Nested
	@DisplayName("CREATE")
	class whenCreate {

		@Test
		@DisplayName("CREATE - When valid - Expected sucess")
		public void create_whenValid_expectedSucess() {
			when(repository.save(Mockito.any())).thenReturn(createUser());
			when(repository.existsById(12345678998L)).thenReturn(Boolean.FALSE);
			var result = service.addClient(createUserDTO());
			assertEquals(UserDTO.fromEntity(createUser()), result);
		}

		@Test
		@DisplayName("CREATE - When invalid - Expected fail")
		public void create_whenInvalid_expectedBadRequest() {
			Assertions.assertThrows(BusinessException.class, () -> {
				when(repository.existsById(12345678998L)).thenReturn(Boolean.TRUE);
				service.addClient(createUserDTO());
			});
		}
	}

	@Nested
	@DisplayName("UPDATE")
	class whenUpdate {

		@Test
		@DisplayName("UPDATE - When valid - Expected sucess")
		public void update_whenValid_expectedSucess() {
			when(repository.save(Mockito.any())).thenReturn(createUser());
			when(repository.existsById(12345678998L)).thenReturn(Boolean.TRUE);
			var result = service.updateClient(12345678998L, createUserDTO());
			assertEquals(UserDTO.fromEntity(createUser()), result);
		}

		@Test
		@DisplayName("UPDATE - When invalid - Expected fail")
		public void update_whenInvalid_expectedBadRequest() {
			Assertions.assertThrows(DataNotFoundException.class, () -> {
				when(repository.existsById(12345678998L)).thenReturn(Boolean.FALSE);
				service.updateClient(12345678998L, createUserDTO());
			});
		}

	}

	private UserDTO createUserDTO() {
		return UserDTO.builder().cpf(12345678998L).nome("Yuri Ferreira").email("teste@teste.com.br")
				.dataNasc("1998/08/03").logradouro("Rua Teste").cidade("Cidade Teste").bairro("Bairro Teste")
				.estado("Estado Teste").cep("CEP Teste").senha("Senha Teste").build();
	}

	private User createUser() {
		return User.builder().cpf(12345678998L).nome("Yuri Ferreira").email("teste@teste.com.br").dataNasc("1998/08/03")
				.logradouro("Rua Teste").cidade("Cidade Teste").bairro("Bairro Teste").estado("Estado Teste")
				.cep("CEP Teste").senha("Senha Teste").build();
	}

	private Optional<User> createOpcionalUser() {
		return Optional.of(createUser());
	}

}
