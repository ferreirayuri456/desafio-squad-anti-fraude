package br.com.bv.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bv.domain.User;
import br.com.bv.domain.UserDTO;
import br.com.bv.service.UserService;

@RunWith(JUnitPlatform.class)
@WebMvcTest(UserController.class)
@DisplayName("Controller - User")
public class UserControllerTest {

	private static final String BASE_URL = "/user/add";
	private static final String BASE_URL_DELETE = "/user/%s/remove";
	private static final String BASE_URL_GET = "/user/%s/list";
	private static final String BASE_URL_PUT = "/user/%s/update";

	private static final String INVALID_REQUEST_BODY = "{}";

	private static final Long VALID_ID = 12564897745L;
	private static final String INVALID_ID = "querty";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService service;

	@Nested
	@DisplayName("CREATE")
	class whenCreate {
		@Test
		@DisplayName("When valid expect 201")
		public void create_whenValid_expectSucess() throws Exception {
			Mockito.when(service.addClient(createSucessPost())).thenReturn(createSucessPost());
			mockMvc.perform(post(String.format(BASE_URL)).contentType(MediaType.APPLICATION_JSON)
					.content(asJsonString(createSucessPost()))).andExpect(status().isCreated());
		}

		@Test
		@DisplayName("When invalid expect 400")
		public void create_whenInvalid_expectFail() throws Exception {
			mockMvc.perform(post(BASE_URL).content(INVALID_REQUEST_BODY).contentType(MediaType.APPLICATION_JSON))
					.andExpect(status().isBadRequest());
		}

	}

	@Nested
	@DisplayName("DELETE")
	class whenDelete {

		@Test
		@DisplayName(value = "DELETE - When valid Expected 204")
		public void delete_whenValid_expectedSucess() throws Exception {
			Mockito.doNothing().when(service).removeClient(VALID_ID);
			mockMvc.perform(delete(String.format(BASE_URL_DELETE, VALID_ID))).andExpect(status().isNoContent());
		}

		@Test
		@DisplayName(value = "DELETE - When invalid Expected 400")
		public void delete_whenInvalid_expectedBadRequest() throws Exception {
			mockMvc.perform(delete(String.format(BASE_URL_DELETE, INVALID_ID))).andExpect(status().isBadRequest());
		}

	}

	@Nested
	@DisplayName("FIND BY ID")
	class whenFindById {

		@Test
		@DisplayName(value = "FIND BY ID - When valid id - Expected 200")
		public void findById_whenValidId_expectedSucess() throws Exception {
			Mockito.when(service.listClientByCPF(VALID_ID)).thenReturn(createSucessGet());
			mockMvc.perform(get(String.format(BASE_URL_GET, 12564897745L))).andExpect(status().isOk());
		}

		@Test
		@DisplayName(value = "FIND BY ID - When invalid id - Expected 400")
		public void findById_whenInvalidId_expectedBadRequest() throws Exception {
			mockMvc.perform(get(String.format(BASE_URL_GET, INVALID_ID))).andExpect(status().isBadRequest());
		}

	}

	@Nested
	@DisplayName("UPDATE")
	class whenUpdate {

		@Test
		@DisplayName(value = "UPDATE - When valid - Expected 200")
		public void update_whenValid_expectedSucess() throws Exception {
			Mockito.when(service.updateClient(VALID_ID, createSucessPut())).thenReturn(createSucessPost());
			mockMvc.perform(put(String.format(BASE_URL_PUT, 12365478998L)).contentType(MediaType.APPLICATION_JSON)
					.content(asJsonString(createSucessPost()))).andExpect(status().isOk());
		}

		@Test
		@DisplayName(value = "UPDATE - When invalid - Expected 400")
		public void update_whenInvalid_expectedBadRequest() throws Exception {
			mockMvc.perform(put(String.format(BASE_URL_PUT, INVALID_ID))).andExpect(status().isBadRequest());
		}

	}

	private UserDTO createSucessPost() {
		return UserDTO.builder().cpf(12345678998L).nome("Yuri Ferreira").email("teste@teste.com.br")
				.dataNasc("1998/08/03").logradouro("Rua Teste").cidade("Cidade Teste")
				.bairro("Bairro Teste").estado("Estado Teste").cep("CEP Teste").senha("Senha Teste").build();

	}

	private UserDTO createSucessPut() {
		return UserDTO.builder().cpf(12345678998L).nome("Yuri Ferreira").email("teste@teste.com.br")
				.dataNasc("1998/08/03").logradouro("Rua Teste").cidade("Cidade Teste")
				.bairro("Bairro Teste").estado("Estado Teste").cep("CEP Teste").senha("Senha Teste").build();

	}

	private User createSucessGet() {
		return User.builder().build();
	}

	private String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
