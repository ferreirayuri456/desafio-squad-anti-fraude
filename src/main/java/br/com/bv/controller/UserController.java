package br.com.bv.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.bv.domain.User;
import br.com.bv.domain.UserDTO;
import br.com.bv.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

	@Autowired
	private UserService service;

	@ApiOperation(value = "Adiciona um cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Cliente criado com sucesso", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Má solicitação para criacao do cliente"),
			@ApiResponse(code = 401, message = "Ausência de autorização"),
			@ApiResponse(code = 404, message = "Cliente não localizado com os parâmetros informados"),
			@ApiResponse(code = 405, message = "Método não permitido"),
			@ApiResponse(code = 500, message = "Sistema indisponível") })
	@PostMapping("/add")
	public ResponseEntity<UserDTO> addClient(
			@ApiParam(value = "Informações do cliente", required = true) @RequestBody @Valid UserDTO dto) {
		log.info("Cadastrando um cliente={}", dto);
		var result = service.addClient(dto);
		return ResponseEntity.status(HttpStatus.CREATED).body(result);
	}

	@ApiOperation(value = "Remove um cliente")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Cliente removido com sucesso", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Má solicitação para remoção do cliente"),
			@ApiResponse(code = 401, message = "Ausência de autorização"),
			@ApiResponse(code = 404, message = "Cliente não localizado com os parâmetros informados"),
			@ApiResponse(code = 405, message = "Método não permitido"),
			@ApiResponse(code = 500, message = "Sistema indisponível") })
	@DeleteMapping("/{cpf}/remove")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteClient(
			@ApiParam(value = "Código do cliente", example = "13254894413", required = true) @PathVariable Long cpf) {
		log.info("Removendo cliente={}", cpf);
		service.removeClient(cpf);
	}

	@ApiOperation(value = "Lista um cliente")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Cliente retornado com sucesso", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Má solicitação para listagem do cliente"),
			@ApiResponse(code = 401, message = "Ausência de autorização"),
			@ApiResponse(code = 404, message = "Cliente não localizado com os parâmetros informados"),
			@ApiResponse(code = 405, message = "Método não permitido"),
			@ApiResponse(code = 500, message = "Sistema indisponível") })
	@GetMapping("/{cpf}/list")
	public ResponseEntity<User> getClientById(
			@ApiParam(value = "Código do cliente", example = "13254894413", required = true) @PathVariable Long cpf) {
		log.info("Buscando cliente pelo cpf={}", cpf);
		var result = service.listClientByCPF(cpf);
		log.info("Cliente encontrado, informações={}", result);
		return ResponseEntity.ok().body(result);
	}

	@ApiOperation(value = "Atualiza um cliente")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Cliente atualizado com sucesso", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Má solicitação para atualização do cliente"),
			@ApiResponse(code = 401, message = "Ausência de autorização"),
			@ApiResponse(code = 404, message = "Cliente não localizado com os parâmetros informados"),
			@ApiResponse(code = 405, message = "Método não permitido"),
			@ApiResponse(code = 500, message = "Sistema indisponível") })
	@PutMapping("/{cpf}/update")
	public ResponseEntity<UserDTO> updateClient(
			@ApiParam(value = "Código do cliente", example = "13254894413", required = true) @PathVariable Long cpf,
			@ApiParam(value = "Informações do cliente", required = true) @RequestBody @Valid UserDTO dto) {
		log.info("Atualizando cliente={}, novo cliente={}", cpf, dto);
		return ResponseEntity.ok(service.updateClient(cpf, dto));

	}

}
