package br.com.bv.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "user")
public class User {

	@Id
	@Column(unique = true)
	private Long cpf;

	@Column(length = 100)
	@Size(min = 2, max = 100)
	private String nome;

	@Column(unique = true)
	@Email(message = "Email inválido")
	private String email;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dataNasc;

	@Size(min = 2, max = 100)
	private String logradouro;

	@Size(min = 2, max = 100)
	private String cidade;

	@Size(min = 2, max = 100)
	private String bairro;

	@Size(min = 2, max = 100)
	private String estado;

	@Size(min = 2, max = 100)
	private String cep;

	private String senha;

	
}
