package br.com.bv.domain;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

	@NotNull
	private Long cpf;
	@NotNull
	private String nome;
	@NotNull
	private String email;
	@NotNull
//	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@JsonFormat(pattern = "yyyy/MM/dd")
	private String dataNasc;
	@NotNull
	private String logradouro;
	@NotNull
	private String cidade;
	@NotNull
	private String bairro;
	@NotNull
	private String estado;
	@NotNull
	private String cep;
	@NotNull
	private String senha;
	
	
	public static User dtoToEntity(UserDTO userDTO) {
		return User.builder()
				.cpf(userDTO.getCpf())
				.nome(userDTO.getNome())
				.email(userDTO.getEmail())
				.dataNasc(userDTO.getDataNasc())
				.logradouro(userDTO.getLogradouro())
				.cidade(userDTO.getCidade())
				.bairro(userDTO.getBairro())
				.estado(userDTO.getEstado())
				.cep(userDTO.getCep())
				.senha(userDTO.getSenha())
				.build();
	}
	
	public static UserDTO fromEntity(User user) {
		return UserDTO.builder()
				.cpf(user.getCpf())
				.nome(user.getNome())
				.email(user.getEmail())
				.dataNasc(user.getDataNasc())
				.logradouro(user.getLogradouro())
				.cidade(user.getCidade())
				.bairro(user.getBairro())
				.estado(user.getEstado())
				.cep(user.getCep())
				.senha(user.getSenha())
				.build();
		
	}

}
