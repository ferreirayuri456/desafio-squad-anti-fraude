package br.com.bv.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommonConstants {

	public static final String NOT_FOUND = "Not Found";
	public static final String DATA_NOT_FOUND = "Nenhum registro encontrado";
	public static final String BAD_REQUEST = "Parâmetros da requisição inválidos";
	
}
