package br.com.bv.service;

import static br.com.bv.exception.BusinessException.builder;

import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.bv.domain.User;
import br.com.bv.domain.UserDTO;
import br.com.bv.exception.DataNotFoundException;
import br.com.bv.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {

	@Autowired
	UserRepository repository;

	@Autowired
	ModelMapper modelMapper;

//	public UserDTO getClient(LocalDate inicialDate, LocalDate finalDate) {
//		var result = repository.find
//	}

	@Transactional
	public UserDTO addClient(UserDTO userDTO) {
		if (!repository.existsById(userDTO.getCpf())) {
			var result = repository.save(UserDTO.dtoToEntity(userDTO));
			return UserDTO.fromEntity(result);
		}
		throw builder().httpStatusCode(HttpStatus.BAD_REQUEST).description("CPF já existente")
				.message("Não foi possível inserir").build();
	}

	@Transactional
	public void removeClient(Long cpf) {
		var result = repository.findById(cpf);
		if (result.isPresent()) {
			repository.deleteById(cpf);
			log.info("Usuário deletado com sucesso, id={}", cpf);
		}
	}

	@Transactional
	public List<?> listClient() {
		var result = repository.findAll();
		log.info("Recuperando todos os usuários={}", result);
		return result;
	}

	@Transactional
	public User listClientByCPF(Long cpf) {
		log.info("Recuperando usuário pelo cpf={}", cpf);
		return repository.findById(cpf).orElseThrow(DataNotFoundException::new);
	}

	@Transactional
	public UserDTO updateClient(Long cpf, UserDTO dto) {
		if (repository.existsById(cpf)) {
			var result = repository.save(UserDTO.dtoToEntity(dto));
			log.info("Atualizando o cliente={}", cpf);
			return UserDTO.fromEntity(result);
		}
		log.info("Cliente não encontrado");
		throw new DataNotFoundException("Not found", "Cliente não encontrado");
	}

}
