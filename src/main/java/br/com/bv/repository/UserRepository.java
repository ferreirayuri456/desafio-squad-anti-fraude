package br.com.bv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.bv.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


}
