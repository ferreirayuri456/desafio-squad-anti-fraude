package br.com.bv.exception;

import org.springframework.http.HttpStatus;

import br.com.bv.util.CommonConstants;

public class DataNotFoundException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataNotFoundException() {
		this.setHttpStatusCode(HttpStatus.NOT_FOUND);
		this.setMessage(CommonConstants.NOT_FOUND);
		this.setDescription(CommonConstants.DATA_NOT_FOUND);
	}

	public DataNotFoundException(String message, String description) {
		this.setHttpStatusCode(HttpStatus.NOT_FOUND);
		this.setMessage(message);
		this.setDescription(description);
	}

}
